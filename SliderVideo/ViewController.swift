//
//  ViewController.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/14.
//

import UIKit

class ViewController: UIViewController {

    lazy var button: UIButton = {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 60))
        btn.setTitle("push", for: .normal)
        btn.backgroundColor = .green
        btn.addTarget(self, action: #selector(pushTo), for: .touchUpInside)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewSets()
        test()
    }

    func viewSets() {
        view.addSubview(button)
        button.center = view.center
    }

    func test() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.pushTo()
        }
    }
    
    @objc func pushTo() {
        let vc = InstanceVC.viewController(InstanceVC.SharePhoto.SliderVC)
        navigationController?.pushViewController(vc, animated: true)
    }
}

