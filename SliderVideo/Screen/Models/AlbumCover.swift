//
//  AlbumCover.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/14.
//

import UIKit
import Photos

enum SourceType {
    case Local
}


class AlbumCover {
    var sourceType: SourceType
    var thumbL: String
    var mediaId: String
    var isThumbReady: Bool
    // for Local
    var phAsset: PHAsset? = nil
    private var _thumbnailImage: UIImage? = nil
    init(sourceType: SourceType, thumbL: String, mediaId: String, isThumbReady: Bool){
        self.sourceType = sourceType
        self.thumbL = thumbL
        self.mediaId = mediaId
        self.isThumbReady = isThumbReady
    }
//    func getLocalThumbnail(_ max: Int = 200) -> UIImage? {
//        guard let asset = PHAsset.fetchAssets(withLocalIdentifiers: [self.mediaId], options: PHFetchOptions()).firstObject else {
//            return nil
//        }
//        return LocalAlbumHelper.shared.getThumbnailByAsset(asset, size: max)
//    }
}
