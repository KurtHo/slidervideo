//
//  BulletComment.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/23.
//

import Foundation

class BulletComment {
    var commentators = [Commentator]()
    
}

class Commentator {
    var name:String
    var content:String
    var time:Date
    var speed:Float = 0
    
    init(name:String, content:String, time:Date) {
        self.name = name
        self.content = content
        self.time = time
    }
}
