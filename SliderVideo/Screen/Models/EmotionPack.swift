//
//  MotionPack.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/14.
//

import Foundation

class EmotionPack {
    enum EmotionType {
        case Happy, ThumbUp, Angry, Love, Sad
    }
    
    var happy:[EmotionType]?
    var thumbUp:[EmotionType]?
    var angry:[EmotionType]?
    var love:[EmotionType]?
    var sad:[EmotionType]?
    
    init(happy:[EmotionType]?, thumup:[EmotionType]?, angry:[EmotionType]?, love:[EmotionType]?, sad:[EmotionType]?) {
        self.happy = happy
        self.thumbUp = thumup
        self.angry = angry
        self.love = love
        self.sad = sad
    }
}



