//
//  ScrollViewCarouselModel.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/15.
//

import UIKit

enum FileType {
    case video, image
}

class ScrollViewCarouselModel {
    var media:[FileTypeModel] = [FileTypeModel]()
}

class FileTypeModel {
    var urlString:String
    var fileType:FileType
    var img: UIImage?
    var index: Int
    init(urlString:String, fileType:FileType, index:Int){
        self.urlString = urlString
        self.fileType = fileType
        self.index = index
    }
}
