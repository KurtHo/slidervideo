import UIKit
import AVKit
import AVFoundation

protocol ScrollViewMechanism:class {
    func current(counter: Int)
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView, player:PlayerView?)
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView, player:PlayerView?)
}

class ScrollViewCarousel: UIScrollView {
    
    weak var svcDelegate: ScrollViewMechanism?
    
    var wait: TimeInterval = 4
    var isMute:Bool = false
    var repeatly:Bool = false {
        didSet {
            self.playerViews.forEach{
                $0.repeatPlay = self.repeatly
            }
        }
    }
    
    weak private var time: Timer?
    var counter = 0
    
    private var files = [FileTypeModel]() {
        didSet {
            self.wait = self.wait + 0 //unable to assign self.wait
            self.addContentView()
        }
    }

    private var bulletComment = BulletComment()
    
    var playerViews = [PlayerView]()
    
    private var scrollable = false {
        didSet{
            if scrollable { timerStart() }
        }
    }
    
    private var albumCovers: [AlbumCover] = [] {
        didSet { self.updateCovers() }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        isPagingEnabled = true
        backgroundColor = .lightGray
        delegate = self
        showsHorizontalScrollIndicator = false
    }
    
    override func draw(_ rect: CGRect) {
        contentSize = CGSize(width: self.frame.width * CGFloat(files.count), height: self.frame.height )
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    //MARK:- Content Sets
    func contentSets(files: [FileTypeModel], isMute:Bool, repeatly:Bool, bulletComment:BulletComment){
        self.files = files
        self.isMute = isMute
        self.repeatly = repeatly
        self.bulletComment = bulletComment
    }
    
    
    private func addContentView(){
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {return}
            self.files.enumerated().forEach { index, file in
                let frame = CGRect(x: 0 + (self.frame.width * CGFloat(index)), y: 0, width: self.frame.width, height: self.frame.height)
                switch file.fileType {
                case .image :
                    let imgView = UIImageView(frame: frame)
                    
                    imgView.image = file.img ?? UIImage()
                    imgView.clipsToBounds = true
                    
                    imgView.isUserInteractionEnabled = true
                    let gesture = UITapGestureRecognizer(target: self, action: #selector(self.imgTapped))
                    imgView.addGestureRecognizer(gesture)
                    
                    imgView.contentMode = .scaleAspectFill
                    self.addSubview(imgView)
                    
                case .video:
                    guard let url = URL(string: file.urlString) else {return}
                    let avPlayer = AVPlayer(url: url)
                    
                    let playerLayer = AVPlayerLayer(player: avPlayer)
                    playerLayer.frame = self.frame
                    let playerView = PlayerView(frame: frame)
                    playerView.setContents(id: file.urlString)
                    
                    playerView.playerLayer.player = avPlayer
                    playerView.delegate = self
                    
                    playerLayer.videoGravity = .resizeAspect
                    self.addSubview(playerView)
                    
                    self.playerViews.append(playerView)
                }
                
                self.contentSize.width = self.frame.width * CGFloat(self.files.count)
                self.layoutIfNeeded()
            }
            
            if self.playerViews.isEmpty == false {
                self.bang(bullets: self.bulletComment, speed: .fast)
                
            }
            
        }
        
    }
    
    @objc private func autoScroll(){
        counter = counter < files.count - 1 ? counter + 1 : 0
        UIView.animate(withDuration: 0.5) {
            self.contentOffset.x = self.frame.width * CGFloat(self.counter)
        }
    }
    
}

//MARK:- Time Controller
extension ScrollViewCarousel {
    private func timerStart(){
        if scrollable && time == nil {
            self.time = Timer.scheduledTimer(timeInterval: wait, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
        }
    }
    
    private func timerShutDown() {
        time?.invalidate()
        time = nil
    }
    
    private func timerContinue(){
        let pageWidth = contentSize.width / CGFloat(files.count )
        let currentPage = contentOffset.x / pageWidth
        counter = Int(currentPage)
        layoutIfNeeded()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            self.timerStart()
        }
    }
}

extension ScrollViewCarousel: UIScrollViewDelegate {
    @objc func imgTapped(){
//        svcDelegate?.current(counter: counter)
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        var player: PlayerView? = nil
        playerViews.forEach{
            $0.isMute = self.isMute
            if $0.id == files[counter].urlString {
                player = $0
            }
        }
        svcDelegate?.scrollViewWillBeginDragging(scrollView, player: player)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        let offset = contentOffset.x
        let frameWidth = frame.width
        let current = CGFloat(offset) / frameWidth
        counter = Int(current)
        print("counter: \(counter)")
        
        timerShutDown()
        timerStart()
        
        var player: PlayerView? = nil
        playerViews.forEach{
            if $0.id == files[counter].urlString {
                player = $0
            }
        }
        
        svcDelegate?.scrollViewDidEndDecelerating(scrollView, player: player)
    }
    
}

//for AlbumCoverUpdate
extension ScrollViewCarousel {
    private func updateCovers() {
        DispatchQueue.main.async {
            
            // Remove old subviews
            self.subviews.forEach { subview in
                subview.removeFromSuperview()
            }
            
            for index in 0..<self.albumCovers.count {
                let imgView = UIImageView(frame: CGRect(x: 0 + (self.frame.width * CGFloat(index)), y: 0, width: self.frame.width, height: self.frame.height))
                
                let c = self.albumCovers[index]
                
                if c.isThumbReady {
                    //Album
//                    imgView.loadXnBayImage(item: c.thumbL, scale: .face, placeHolder: defaultGray)
                } else {
                    
                    if c.sourceType == SourceType.Local {
                        //Local
//                        let image = c.getLocalThumbnail(480) ?? defaultGray
//                        imgView.image = image
                        imgView.backgroundColor = .gray
                    } else  {
                        //XnBay
//                        imgView.loadXnBayImage(item: c.thumbL, scale: .face, placeHolder: defaultGray)
                    
                    }
                }
                imgView.clipsToBounds = true
                imgView.contentMode = .scaleAspectFill
                self.addSubview(imgView)
            }
            self.contentSize.width = self.frame.width * CGFloat(self.albumCovers.count)
            self.layoutIfNeeded()
        }
    }
}

extension ScrollViewCarousel: PlayerViewDelegate {
    func toMute(_ mute: Bool) {
        self.isMute = mute
    }
}
