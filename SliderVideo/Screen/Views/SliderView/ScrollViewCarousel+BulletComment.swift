//
//  ScrollViewCarousel+BulletComment.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/28.
//

import UIKit

enum BulletSpeed:CGFloat {
    case slow   = 5
    case medium = 4
    case fast   = 3
}

extension ScrollViewCarousel {
    
    func bang(bullets: BulletComment, speed: BulletSpeed){
        var delay: TimeInterval = 0
        
        bullets.commentators.enumerated().forEach{ number, _ in
            delay += TimeInterval.random(in: 0.3...1)
            
            
            let x = UIScreen.main.bounds.width
            
            let y = CGFloat.random(in: 20...(self.frame.height / 3))
//            let y = self.frame.height - CGFloat.random(in: (frame.height / 3 * 2)...400)
            let contentView = UIView(frame: CGRect(x: x, y: y, width: 200, height: 5))
            
            
            let lbName = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 60))
            lbName.setFont(fontName: .PingFangTCSemibold, size: 14, color: .white)
            lbName.text =  bullets.commentators[number].name + "："
            lbName.numberOfLines = 1
            lbName.sizeToFit()
            

            let lbContent = UILabel(frame: CGRect(x: 0 + lbName.frame.width, y: 0 , width: 100, height: 60))
            lbContent.setFont(fontName: .PingFangTCRegular, size: 14, color: .white)
            lbContent.text =  bullets.commentators[number].content
            lbContent.numberOfLines = 1
            lbContent.sizeToFit()
            
            contentView.frame.size.width = lbContent.frame.width + lbName.frame.width
            
            contentView.addSubview(lbName)
            contentView.addSubview(lbContent)
            
            let duration: TimeInterval = {
                let end = contentView.frame.width + contentView.frame.origin.x
                let divided = x / speed.rawValue
                return TimeInterval(end / divided)
            }()
            
            self.superview?.addSubview(contentView)

            UIViewPropertyAnimator.runningPropertyAnimator(withDuration: duration, delay: delay, options: .curveLinear, animations: {
                contentView.frame.origin.x = 0 - contentView.frame.size.width
            }, completion: { _ in
                contentView.removeFromSuperview()
            })
            
            
        }
        
        
        
        
        
    }
}
