import UIKit
import AVKit

protocol PlayerViewDelegate: class {
    func toMute(_ mute:Bool)
}

class PlayerView: UIView {
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self
    }

    var playerLayer: AVPlayerLayer {
        get {
            return layer as! AVPlayerLayer
        }
        set {}
    }
    
    var player: AVPlayer? {
        get {
            return playerLayer.player
        }
        set {
            playerLayer.player = newValue
        }
    }
    
    var id: String = ""
    var isPlaying:Bool = true {
        didSet {
            self.playWithBtn(show: self.isPlaying == false)
        }
    }
    var pausedByUser:Bool = false {
        didSet {
            self.playWithBtn(show: self.isPlaying )
        }
    }
    var repeatPlay:Bool = true
    var isMute:Bool = false {
        didSet {
            self.player?.isMuted = isMute
            if isMute {
                self.muteBtn.setImage(.iconVolumeOffLarge, for: .normal)
            }else {
                self.muteBtn.setImage(.volume, for: .normal)
            }
        }
    }
    
    var endOfPlay:Bool = false {
        didSet(newValue) {
            if endOfPlay {
                self.playerBtn.isHidden = false
            }
            if newValue {
                self.player?.seek(to: .zero)
            }
        }
    }
    weak var delegate:PlayerViewDelegate?
    
    
    private lazy var playerBtn = UIButton()
    private lazy var muteBtn = UIButton()
    
    
    func setContents(id:String){
        self.id = id
        
        isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(tapScreen))
        addGestureRecognizer(gesture)
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self, let superView = self.superview else {return}
            self.playerBtn.frame.size.height = superView.frame.width / 6.7
            self.playerBtn.frame.size.width = self.playerBtn.frame.size.height
            self.playerBtn.center = superView.center
            self.playerBtn.setImage(.iconPlayLarge, for: .normal)
            self.playerBtn.addTarget(self, action: #selector(self.videoPlay), for: .touchUpInside)
            self.addSubview(self.playerBtn)
            
            self.muteBtn.frame.size.height = self.playerBtn.frame.height / 1.4
            self.muteBtn.frame.size.width = self.muteBtn.frame.height
            
            let offset = superView.frame.height - 12 - self.muteBtn.frame.height
            self.muteBtn.frame.origin = CGPoint(x: offset, y: offset)
            self.muteBtn.imageEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
            
            self.muteBtn.setImage(.volume, for: .normal)
            self.muteBtn.addTarget(self, action: #selector(self.videoMute), for: .touchUpInside)
            self.addSubview(self.muteBtn)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(didFinishPlaying(note:)), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        
    }
    
    deinit {
        player?.pause()
        player = nil
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func didFinishPlaying(note: NSNotification) {
        guard let item = note.object as? AVPlayerItem else {return}
        if item.asset == player?.currentItem?.asset {
            if repeatPlay {
                self.player?.seek(to: .zero)
                self.player?.play()
            }else {
                self.endOfPlay = true
            }
        }
        
    }
    
    @objc func videoPlay() {
        if endOfPlay {
            self.endOfPlay = false
        }
        isPlaying = true
        
    }
    
    @objc func videoPause() {
//        self.player?.pause()
        isPlaying = false
    }
    
    @objc func videoMute() {
        
        if muteBtn.imageView?.image == .volume {
            self.delegate?.toMute(true)
            player?.isMuted = true
            muteBtn.setImage(.iconVolumeOffLarge, for: .normal)
        }else if muteBtn.imageView?.image == .iconVolumeOffLarge {
            self.delegate?.toMute(false)
            player?.isMuted = false
            muteBtn.setImage(.volume, for: .normal)
        }
    }
    
    @objc func tapScreen() {
        pausedByUser = !pausedByUser
    }
    
    func scrollAndJudgePlay(toPlay:Bool){
        if pausedByUser == false {
            if toPlay && endOfPlay == false {
                self.player?.play()
                self.playerBtn.isHidden = true
            }else {
                self.player?.pause()
                self.playerBtn.isHidden = false
            }
        }
    }
}
//MARK:- Pirvate
extension PlayerView {
    private func playWithBtn(show: Bool) {
        if show {
            self.player?.pause()
            self.playerBtn.isHidden = false
        }else {
            self.player?.play()
            self.playerBtn.isHidden = true
        }
    }
}
