//
//  CommentView.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/14.
//

import UIKit

class CommentView: UIView, NibOwnerLoadable {
    @IBOutlet weak var ivOne: UIImageView!
    @IBOutlet weak var ivTwo: UIImageView!
    @IBOutlet weak var ivThree: UIImageView!
    @IBOutlet weak var ivFour: UIImageView!
    @IBOutlet weak var ivFive: UIImageView!
    @IBOutlet weak var commentUser: UILabel!
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadNibContent()
        viewSets()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNibContent()
        viewSets()
    }
    
    func viewSets() {
        ivOne.image = UIImage.iconAngrySmall
        ivTwo.image = UIImage.iconCrySmall
        ivThree.image = UIImage.iconThumbUpSmall
        ivFour.image = UIImage.iconHappySmall
        ivFive.image = UIImage.iconHeartSmall
        commentUser.setFont(fontName: .PingFangTCRegular, size: 14, color: .cmtGray)
        commentUser.text = "Phet Putrie和其他99人"
    }
    
}
