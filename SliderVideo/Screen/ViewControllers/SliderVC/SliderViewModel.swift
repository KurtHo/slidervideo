//
//  SliderViewModel.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/14.
//

import UIKit

class SliderViewModel {
    
    var files:[FileTypeModel] = [
        FileTypeModel(urlString: "https://i.pinimg.com/564x/ba/a3/07/baa307a7de3030f0073c56fa95ab2a3c.jpg", fileType: .image, index: 0),
        FileTypeModel(urlString: "https://v.pinimg.com/videos/720p/77/4f/21/774f219598dde62c33389469f5c1b5d1.mp4", fileType: .video, index: 0),
        FileTypeModel(urlString: "https://i.pinimg.com/564x/ef/17/51/ef17519f5e473adc01dfd64c35cf44d4.jpg", fileType: .image, index: 0),
        FileTypeModel(urlString: "https://i.pinimg.com/564x/6f/5f/fb/6f5ffb82a1f9a9f7e478b8a2486831f5.jpg", fileType: .image, index: 0),
        FileTypeModel(urlString: "https://v.pinimg.com/videos/720p/75/40/9a/75409a62e9fb61a10b706d8f0c94de9a.mp4", fileType: .video, index: 0),
        
        FileTypeModel(urlString: "https://v.pinimg.com/videos/720p/62/81/60/628160e025f9d61b826ecc921b9132cd.mp4", fileType: .video, index: 0),
        FileTypeModel(urlString: "https://v.pinimg.com/videos/720p/5f/aa/3d/5faa3d057eb31dd05876f622ea2e7502.mp4", fileType: .video, index: 0),
        FileTypeModel(urlString: "https://v.pinimg.com/videos/720p/65/b0/54/65b05496c385c89f79635738adc3b15d.mp4", fileType: .video, index: 0),
        FileTypeModel(urlString: "https://i.pinimg.com/564x/3c/52/d3/3c52d31a1b388ea584175f7859fb23e7.jpg", fileType: .image, index: 0),
        FileTypeModel(urlString: "https://i.pinimg.com/564x/4c/32/ee/4c32ee62af42bacec8c50ddfd10ade63.jpg", fileType: .image, index: 0),
    ]
        
    var bulletComment:BulletComment = {
        var bc = BulletComment()
        let c1 = Commentator(name: "Trump", content: "我體內流的不是DNA，而是USA", time: Date.daysAgo(day: -1))
        let c2 = Commentator(name: "Biden", content: "我是來參選議員的", time: Date.daysAgo(day: -2))
        let c3 = Commentator(name: "Obama", content: "歐巴馬", time: Date.daysAgo(day: -3))
        let c4 = Commentator(name: "布希", content: "I wanna know，你希布希", time: Date.daysAgo(day: -3))
        bc.commentators = [c1,c2,c3,c4]
        return bc
    }()
    
    var urlStr:String {
       return "https://picsum.photos/\(testArchForRandom(start:200, end: 300))/\(testArchForRandom(start:200, end: 300))"
    }
    
    lazy var urlStrs = [urlStr,urlStr,urlStr,urlStr]
    
    private func testArchForRandom(start:Int, end:Int) -> Int{
        return Int.random(in: start...end)
    }
}

