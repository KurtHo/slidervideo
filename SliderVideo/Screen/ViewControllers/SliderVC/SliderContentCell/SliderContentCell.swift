//
//  SliderContentCell.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/14.
//

import UIKit
import AVKit

class SliderContentCell: UITableViewCell {
    @IBOutlet weak var slider: ScrollViewCarousel!
    @IBOutlet weak var pageControl: UIPageControl!

    @IBOutlet weak var commentView: CommentView!
    @IBOutlet weak var commentCountLb: UILabel!
    
    private var bulletComment = BulletComment()
    
    private var commentsCount:Int = 0 {
        didSet {
            self.commentCountLb.text = "\(self.commentsCount)則彈幕"
        }
    }
    private var emotionPack:EmotionPack?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setViews()
    }

    func setViews() {
        selectionStyle = .none 
        commentCountLb.setFont(fontName: .PingFangTCRegular, size: 14, color: .cmtGray)
        commentCountLb.text = ""
        
        pageControl.pageIndicatorTintColor = .cmtGray
        pageControl.currentPageIndicatorTintColor = .black
        
        slider.svcDelegate = self
        
    }
    
    func setContents(imgStrs:[FileTypeModel], emotionPack:EmotionPack?, commentsCount:Int, bulletComment: BulletComment){
        self.commentsCount = commentsCount
        self.emotionPack = emotionPack
        self.bulletComment = bulletComment
        
        var counter = 0
        let files = imgStrs
        
        func sliderContentSets() {
            counter += 1
            if counter == files.count {
                self.slider.contentSets(files: files, isMute: false, repeatly: true, bulletComment: self.bulletComment)
            }
        }
        
        imgStrs.enumerated().forEach{ index, imgStr in
            if imgStr.fileType == .image {
                UIImageView().kfSetImage(str: imgStr.urlString, defaultImg: UIImage()) { (img) in
                    files[index].img = img
                    
                    sliderContentSets()
                }
            }else {
                sliderContentSets()
            }
        }
        pageControl.numberOfPages = imgStrs.count
    }
    
    
}

extension SliderContentCell: ScrollViewMechanism {
    func current(counter: Int) {
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView, player: PlayerView?) {
//        currentPlayer?.pause()
//        player?.videoPause()
//        currentPlayer = player?.player
        
        player?.scrollAndJudgePlay(toPlay: false)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView, player: PlayerView?) {
        pageControl.currentPage = slider.counter
//        player?.play()
//        player?.videoPlay()
//        currentPlayer = player?.player
        player?.scrollAndJudgePlay(toPlay: true)
    }
    
    
}
