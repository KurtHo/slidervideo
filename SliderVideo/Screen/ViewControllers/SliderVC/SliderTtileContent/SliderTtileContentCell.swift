//
//  SliderTtileContent.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/14.
//

import UIKit

class SliderTtileContentCell: UITableViewCell {
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var poster: UILabel!
    @IBOutlet weak var titleContent: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var option: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewSets()
    }

    func viewSets() {
        selectionStyle = .none 
        thumbnail.kfSetImage(str: "https://picsum.photos/200/300", defaultImg: UIImage() )
        thumbnail.toCircle()
        
        poster.setFont(fontName: .PingFangTCRegular, size: 12, color: .black)
        poster.text = "Random-Photo"
        
        titleContent.setFont(fontName: .PingFangTCMedium, size: 12, color: .black)
        titleContent.text = "全家去家族旅行-20200425日本北海道老少閒宜-全體家族照"
        titleContent.numberOfLines = 2
        titleContent.lineBreakMode = .byCharWrapping
        
        dateLabel.setFont(fontName: .PingFangTCRegular, size: 12, color: .darkGray)
        dateLabel.text = "昨天 18:30"
        
    }
    
}
