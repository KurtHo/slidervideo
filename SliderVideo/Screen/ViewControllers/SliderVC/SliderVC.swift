//
//  SliderVC.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/14.
//

import UIKit

class SliderVC: UIViewController {
    
    let vm = SliderViewModel()
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        setTableView()
    }
    
    func setViews() {
//        vm.urlStrs.forEach {
//            UIImageView().kfSetImage(str: $0, defaultImg: UIImage())
//        }
    }
    
    func setTableView(){
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(SliderContentCell.nib, forCellReuseIdentifier: SliderContentCell.identifier)
        tableView.register(SliderTtileContentCell.nib, forCellReuseIdentifier: SliderTtileContentCell.identifier)
    }
    
}

extension SliderVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 72
        default:
            return self.view.frame.width + 73
        }
    }
}

extension SliderVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: SliderTtileContentCell.identifier) as! SliderTtileContentCell
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: SliderContentCell.identifier) as! SliderContentCell
            
            cell.setContents(imgStrs: vm.files, emotionPack: nil, commentsCount: 5, bulletComment: vm.bulletComment)
            return cell
        }
    }
    
    
}
