//
//  InstanceVC.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/14.
//

import UIKit

protocol VCMap {
    var storyBoardId: String {get}
    var identifier: String {get}
}

class InstanceVC {
    static func viewController(_ vc: VCMap) -> UIViewController {
        let sb = UIStoryboard(name: vc.storyBoardId, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: vc.identifier)
        return vc
    }
    
    enum SharePhoto: VCMap {
        case SliderVC
        
        var storyBoardId: String {
            switch self {
            case .SliderVC:
                return "Main"
            }
        }
        
        var identifier: String {
            switch self {
            case .SliderVC:
                return "SliderVC"
            
            }
        }
    }
    
}

