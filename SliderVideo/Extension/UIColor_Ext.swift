//
//  Color.swift
//  MusicPart
//
//  Created by CI-Kurt on 2019/10/22.
//  Copyright © 2019 CI-Kurt. All rights reserved.
//

import UIKit

extension UIColor {
    class var primary: UIColor {
        return UIColor(red: 68/255, green: 195/255, blue: 206/255, alpha: 1)
    }
}

// MARK: - Font Color


extension UIColor {
    static let albumBlack = UIColor(red: 17/255, green: 17/255, blue: 17/255, alpha: 1.0)
    
    static let theme = UIColor(red: 42/255, green: 178/255, blue: 218/255, alpha: 1.0)
    
    static let placeHolder = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1.0)
    
    static let wordsUnselect = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
    
    static let underline = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1.0)
    
    static let naviUderline = UIColor(red: 221/255, green: 221/255, blue: 221/255, alpha: 1.0)
    
    static let fakeNaviUnderline = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
    
    static let bottomRoundedLabel = UIColor(red: 68/255, green: 68/255, blue: 68/255, alpha: 1.0)
    
    static let textBlack = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1.0)
    
    static let dateBlack = UIColor(red: 104/255, green: 102/255, blue: 102/255, alpha: 1.0)
    
    static let cmtGray = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1.0)
    
    static var textBlackInDarkMode:UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "textBlackInDarkMode") ?? .black
        } else {
            return .black
        }
    }
    
    static let sliderUnprogress = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1.0)
    
    static let seperator = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1.0)
    
}

