import UIKit
import Kingfisher

extension UIImageView {
    enum Align {
        case vertical, horizon
    }
    
    func toCircle() {
        self.contentMode = .scaleAspectFill
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
    }
    
    
    func kfSetImage(str: String?, defaultImg: UIImage, finish: ( (UIImage)->Void)? = nil ){
        if let urlstr = str?.noChinese(), let url = URL(string: urlstr) {
            
            self.kf.setImage(with: url, placeholder: defaultImg, completionHandler:  { result in
                switch result {
                case .success:
                    finish?(self.image ?? defaultImg)
                    
                case .failure:
                    finish?(defaultImg)
                }
            })
            
        }else {
            self.image = defaultImg
        }
    }
    
    func resizeTo(align: Align){
        guard let image = self.image else {return }
        let ratio = image.size.width / image.size.height
        
        switch align {
        case .vertical:
            if frame.width > frame.height {
//                let newWidth = frame.height * ratio
//                frame.size = CGSize(width: newWidth, height: frame.height)
            }else {
                let newHeight = frame.width / ratio
                frame.size = CGSize(width: frame.width, height: newHeight)
            }
            
        case .horizon:
            if frame.width > frame.height {
                let newHeight = frame.width / ratio
                frame.size = CGSize(width: frame.width, height: newHeight)
                
            }else {
                let newWidth = frame.height / ratio
                frame.size = CGSize(width: newWidth, height: frame.height)
            }
        }
    }

}
