//
//  UITableViewCell_Ext.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/14.
//

import UIKit

extension UITableViewCell {
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
}
