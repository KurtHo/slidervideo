//
//  String_Ext.swift
//  MusicPart
//
//  Created by CI-Kurt on 2019/11/5.
//  Copyright © 2019 CI-Kurt. All rights reserved.
//

import Foundation

extension String {
    func noChinese() -> String?{
        return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    }
    
    var floatValue: Float {
        return (self as NSString).floatValue
    }
 
    
    func cutStr(from: Int, to: Int)-> String{
        if self.count < from || self.count < abs(to) { return "" }
        let start = self.index(self.startIndex, offsetBy: from)
        let end = self.index(self.endIndex, offsetBy: to)
        let result = self[start...end]
        return String(result)
        
    }
}

