//
//  UIImage_Ext.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/14.
//

import UIKit

extension UIImage {
    static let iconAngrySmall:UIImage = UIImage(imageLiteralResourceName: "iconAngrySmall")
    
    static let iconCrySmall:UIImage = UIImage(imageLiteralResourceName: "iconCrySmall")
    
    static let iconHappySmall:UIImage = UIImage(imageLiteralResourceName: "iconHappySmall")
    
    static let iconHeartSmall:UIImage = UIImage(imageLiteralResourceName: "iconHeartSmall")
    
    static let iconThumbUpSmall:UIImage = UIImage(imageLiteralResourceName: "iconThumbUpSmall")
    
    static let iconPlayLarge:UIImage = UIImage(imageLiteralResourceName: "iconPlayLarge")
    
    static let volume:UIImage = UIImage(imageLiteralResourceName: "volume")
    
    static let iconVolumeOffLarge:UIImage = UIImage(imageLiteralResourceName: "iconVolumeOffLarge")
    
}
