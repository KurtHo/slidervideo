//
//  Float_Ext.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/23.
//

import Foundation

extension Float {
    func getRandom(_ from:Float, to:Float) -> Float {
        return Float.random(in: from...to)
    }
}
