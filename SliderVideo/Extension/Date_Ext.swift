//
//  Date_Ext.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/10/28.
//

import Foundation

//testOnly
extension Date {
//    static var yesterday: Date { return Date().dayBefore }
//    static var tomorrow:  Date { return Date().dayAfter }
//    var dayBefore: Date {
//        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
//    }
//    var dayAfter: Date {
//        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
//    }
    
//    var month: Int {
//        return Calendar.current.component(.month,  from: self)
//    }
//    var isLastDayOfMonth: Bool {
//        return dayAfter.month != month
//    }
    
    static func daysAgo(day:Int) -> Date{
        var noon: Date {
            return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: Date())!
        }
        return Calendar.current.date(byAdding: .day, value: day, to: noon)!
    }
}
