# Demo Only

#   目錄 
1. 環境
2. 版本邏輯
3. 套件
4. 設計模式(Design Pattern)
5. 需求
6. 歷史

------

## 1. 環境
支援ios 10.0 以上版本 (建議11.0以上)
設計不包含
* 裝置：iPad
* 顯示方式：橫屏顯示、Dark Mode

------

## 2. 版本邏輯
* 依照現有的zeplin，若為bug <br>
    小版號自動新增，大版號不變 <br>
    例：1.0.2 (10) → 1.0.2 (11) 
* 若不在上述的邏輯或 衝突 <br>
    大版號自動新增，小版號歸零 <br>
    例：1.0.2 (10) → 1.0.3 (0)

------

## 3. 套件
* Kingfisher    <https://github.com/onevcat/Kingfisher/>
 
 ------
 
 ## 4. 設計模式(Design Pattern)
 * MVVM  <https://www.appcoda.com.tw/mvvm-vs-mvc//>
    Model = 資料格式，基本功能 <br>
    View = 畫面的設置 <br>
    View Model = 透過vc更動model，若有需要則callback回vc進而更改view <br>
    
 ------

 ## 5. 需求
   slider vc:<br>
    a. 僅播放在中間的player view 圖/影，若是影片只播放15秒 <br>
    b. 黑色狗狗為預設的空圖片(例如資源找不到或錯誤) <br>
    c. 靜音時統一靜音 <br>
    d. 點擊影片時從該時間接續播放 <br>
    e. 左右滑動時，影片停止播放 <br>
    f. 記下左右上下滑動時，現在的內容 <br>
<br>
   emotion vc: <br>
    a. 表情飄浮方向/角度隨機 <br>
    b. 彈幕隨機速度 <br>
    c. 每一則結束的彈幕，下滑上滑時就不再播放 <br>

